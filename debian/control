Source: ruby-oedipus-lex
Section: ruby
Priority: optional
Maintainer: Debian Ruby Extras Maintainers <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Lucas Kanashiro <kanashiro@debian.org>
Build-Depends: debhelper-compat (= 13),
               gem2deb (>= 1),
               rake
Standards-Version: 4.6.1
Vcs-Git: https://salsa.debian.org/ruby-team/ruby-oedipus-lex.git
Vcs-Browser: https://salsa.debian.org/ruby-team/ruby-oedipus-lex
Homepage: http://github.com/seattlerb/oedipus_lex
Testsuite: autopkgtest-pkg-ruby
XS-Ruby-Versions: all
Rules-Requires-Root: no

Package: ruby-oedipus-lex
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
Depends: ${misc:Depends},
         ${ruby:Depends},
         ${shlibs:Depends}
Description: Lexer generator in the same family as Rexical and Rex
 Oedipus Lex is a lexer generator in the same family as Rexical and
 Rex. Oedipus Lex is a independent lexer fork of Rexical. Rexical was
 in turn a fork of Rex. We've been unable to contact the author of rex
 in order to take it over, fix it up, extend it, and relicense it to
 MIT. So, Oedipus was written clean-room in order to bypass licensing
 constraints (and because bootstrapping is fun).
 .
 Oedipus brings a lot of extras to the table and at this point is only
 historically related to rexical. The syntax has changed enough that
 any rexical lexer will have to be tweaked to work inside of oedipus.
 At the very least, you need to add slashes to all your regexps.
 .
 Oedipus, like rexical, is based primarily on generating code much like
 you would a hand-written lexer. It is _not_ a table or hash driven
 lexer. It uses StrScanner within a multi-level case statement. As such,
 Oedipus matches on the _first_ match, not the longest (like lex and
 its ilk).
